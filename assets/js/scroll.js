$('.homeScroll').click(function () {
    $('html, body').animate({
        scrollTop: $('.navbar').offset().top
    }, 800);
});

$('.speakersScroll').click(function () {
    $('html, body').animate({
        scrollTop: $('.speakers').offset().top
    }, 800);
});

$('.programScroll').click(function () {
    $('html, body').animate({
        scrollTop: $('.programs-general').offset().top
    }, 800);
});

$('.workshopScroll').click(function () {
    $('html, body').animate({
        scrollTop: $('.programs').offset().top
    }, 800);
});

$('.workshops-link').click(function () {
    $('html, body').animate({
        scrollTop: $('.title-workshops').offset().top
    }, 800);
});

$('.contactScroll').click(function () {
    $('html, body').animate({
        scrollTop: $('.contact-us-section').offset().top
    }, 800);
});

// contact-us-section