'use strict'
var nave = document.querySelector('.navbar');
var logo = document.querySelector('.logo');
var logoDark = document.querySelector('.logo-dark');
var status;

function changeColor() {
    var scroll = document.body.scrollTop || document.documentElement.scrollTop;
    if (scroll >= 200 && status == null) {
        status = true;
        changeColor();
    } else {
        status = false;
    }
}


function changeColor() {
    nave.classList.add('light');
    logo.classList.add('logo-hidden');
    logoDark.classList.add('logo-show');
}


window.addEventListener('load', changeColor);