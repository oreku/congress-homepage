const btnMenu = document.getElementById('btn-menu')
const mobileMenu = document.getElementById('mobile-menu')

var flag = 0;

btnMenu.addEventListener('click', function () {
    if (flag == 0) {
        mobileMenu.style.top = '4rem';
        flag = 1;
    } else {
        mobileMenu.style.top = '-100vh';
        flag = 0;
    }  
})